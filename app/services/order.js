import Ember from 'ember';

export default Ember.Service.extend({

  findMaxOrder(collection){
    let maxOrder = collection.sortBy('order').get('lastObject.order');

      if(typeof maxOrder !== 'number'){
        maxOrder = 0;
      }

      return maxOrder + 1;
  },

  moveDown(item, collection){

    let currentOrder = item.get('order');

    var itemBelow = collection.sortBy('order').find(item => {
      return item.get('order') > currentOrder;
    });

    if(itemBelow){

      item.set('order', itemBelow.get('order'));
      itemBelow.set('order', currentOrder);

      item.save();
      itemBelow.save();
    }
  },

  moveUp(item, collection){

      var currentOrder = item.get('order');
      var itemAbove = collection.sort((prev, next) => {
        return next.get('order') > prev.get('order');
      }).find(item => {
        return item.get('order') < currentOrder;
      });

      if(itemAbove){
        item.set('order', itemAbove.get('order'));
        itemAbove.set('order', currentOrder);

        item.save();
        itemAbove.save();
      }

  }




});
