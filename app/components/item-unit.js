import Ember from 'ember';

export default Ember.Component.extend({
	isEditing: false,
	moveUpAction: 'moveItemUp',
	moveDownAction: 'moveItemDown',
	actions: {
		save(){
			var self = this;
			this.get('model').save().then(function(){
				self.set('isEditing', false);
			});
		},
		edit(){
			this.toggleProperty('isEditing');
		},
		delete(){
			if(confirm("opravdu smazat?")){
				this.get('model').destroyRecord();
			}
		},
		moveUp(){
			this.sendAction('moveUpAction', this.get('model'));
		},
		moveDown(){
			this.sendAction('moveDownAction', this.get('model'));
		},
		cancel(){
			this.get('model').rollbackAttributes();
			this.set('isEditing', false);
		}
	}
});
