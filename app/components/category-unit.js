import Ember from 'ember';

const {inject} = Ember;

export default Ember.Component.extend({
	isEditing: false,

	order: inject.service(),

	itemsSorting: ['order'],
	sortedItems: Ember.computed.sort('model.items','itemsSorting'),
	newItem: Ember.computed(function(){
		return {};
	}),

	addItemAction: 'addItem',
	isLoading: false,
	// sortedItems: Ember.computed.alias('model'),

	actions: {
		edit(){
			this.toggleProperty('isEditing');
		},
		save(){
			var self = this;
			this.get('model').save().then(function(){
				self.set('isEditing', false);
			});
		},
		delete(){
			if(confirm("Opravdu smazat?")){
				var category = this.get('model');
				category.get('items').forEach(function(item){
					item.destroyRecord();
				});
				category.destroyRecord();
			}
		},
		moveItemUp(item){
			this.get('order').moveUp(item, this.get('sortedItems'));
		},
		moveItemDown(item){
			this.get('order').moveDown(item, this.get('sortedItems'));
		},
		addItem(){
			this.set('newItem.category', this.get('model'));

			let maxOrder = this.get('order').findMaxOrder(this.get('sortedItems'));

			this.set('newItem.order', maxOrder);
			// console.log(this.get('newItem'));
			this.set('isLoading', true);
			var self = this;
			this.sendAction('addItemAction', this.get('newItem'), this);
		},

		moveLeft(){
			this.get('moveLeft')(this.get('model'));
		},

		moveRight(){
			this.get('moveRight')(this.get('model'));
		}
	}
});
