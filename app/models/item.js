import DS from 'ember-data';

export default DS.Model.extend({
	category: DS.belongsTo('category'),
	name: DS.attr('string'),
	amount: DS.attr('string'),
	price: DS.attr('string'),
	weight: DS.attr('string'),
	order: DS.attr('number')
  
});
