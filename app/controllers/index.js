import Ember from 'ember';

const {inject} = Ember;

export default Ember.Controller.extend({

	order: inject.service(),

	section: 'jidlo',
	queryParams: ['section'],
	jidloCategories: Ember.computed.filterBy('model','section', 'jidlo'),
	napojeCategories: Ember.computed.filterBy('model','section','napoje'),
	categories: Ember.computed('section','jidloCategories.@each.order','napojeCategories.@each.order', function(){
		var section = this.get('section')+'Categories';
		// console.log(section);
		var categories = this.get(section).sortBy('order');
		// console.log(categories);
		return categories;
	}),
	isJidlo: Ember.computed.equal('section','jidlo'),
	isNapoje: Ember.computed.equal('section','napoje'),

	alert: {},
	newCategory: {},
	isLoading: false,
	isUpdatingMenu: false,
	fileName: "menu",

	clearAlert(){
		this.set('alert', {});
	},

	makeArray(responseObject){
		let arr = [];
		for(let cat in responseObject){
			let catObject = responseObject[cat];
			catObject.id = cat;
			arr.push(catObject);
		}

		return arr;
	},

	findItemsForCategory(category, items){
		return items.filterBy('category', category.id).sortBy('order');
	},

	actions: {
		addCategory(){
			var newCategory = this.get('newCategory');
			newCategory.section = this.get('section');

			newCategory.order = this.get('order').findMaxOrder(this.get('categories'));

			this.set('isLoading', true);
			var self = this;
			this.store.createRecord('category', newCategory).save().then(function(){
				self.set('isLoading', false);
			});
		},
		addItem(newItem, component){
			// console.log(newItem);
			var category = newItem.category;
			return this.store.createRecord('item', newItem).save().then(function(){

				return category.save().then(function(){
					component.set('isLoading', false);
				});

			});

		},

		moveCategoryLeft(category){
			this.get('order').moveUp(category, this.get('categories'));
		},

		moveCategoryRight(category){
			this.get('order').moveDown(category, this.get('categories'));
		},

		updateMenu(){
			var dataRequests = [];
			var data = {};
			this.set('isUpdatingMenu', true);
			var self = this;
			this.clearAlert();
			dataRequests.pushObject(Ember.$.getJSON('http://burning-torch-259.firebaseio.com/items.json').then(items => {
				data.items = this.makeArray(items);
			}));

			dataRequests.pushObject(Ember.$.getJSON('http://burning-torch-259.firebaseio.com/categories.json?orderBy="section"&startAt="jidlo"&endAt="jidlo"').then(categories => {
				data.jidlo = this.makeArray(categories).sortBy('order');
			}));

			dataRequests.pushObject(Ember.$.getJSON('http://burning-torch-259.firebaseio.com/categories.json?orderBy="section"&startAt="napoje"&endAt="napoje"').then(categories => {
				data.napoje = this.makeArray(categories).sortBy('order');
			}));


			Ember.RSVP.all(dataRequests).then(() => {

				data.jidlo.forEach(category => {
					category.items = this.findItemsForCategory(category, data.items);
				});

				data.napoje.forEach(category => {
					category.items = this.findItemsForCategory(category, data.items);
				});
				Ember.$.post('../serve.php', data).then(function(){
					self.set('isUpdatingMenu', false);
					self.set('alert.message', 'Menu aktualizovano uspesne');
					self.set('alert.type', 'positive');
				});

				console.log(data);
			});
		},
		uploadMenu(file){
			file.file.name = this.get('fileName')+".pdf";
			this.clearAlert();
			var self = this;
			file.upload('../upload/index.php').then(function(response){
				self.set('alert.message', 'Soubor nahran uspesne');
				self.set('alert.type', 'positive');
			});
		}
	}
});
